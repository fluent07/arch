#!/bin/bash

pacman-key --init
pacman-key --populate archlinux
pacman --noconfirm -S gum
timezones=$(find /usr/share/zoneinfo -maxdepth 1 -type d -printf "%f\n" | tail -n +2)  # Exclude the current directory
continent=$(echo "$timezones" | gum choose --item.foreground=34 --selected.foreground=412)
cities=$(find /usr/share/zoneinfo/$continent -maxdepth 1 -printf "%f\n" | tail -n +2)  # Exclude the current directory
city=$(echo "$cities" | gum choose --item.foreground=34 --selected.foreground=412)
# Handle the user's choice
if [ -n "$city" ]; then
    echo "You selected: $continent-$city"
else
    echo "No timezone selected."
fi

echo "Enter hostname: "
read hostname

echo "Enter Username: NO CAPITALS"
read username

# ------------------------------------------------------
# Set System Time
# ------------------------------------------------------
ln -sf /usr/share/zoneinfo/$zoneinfo /etc/localtime
hwclock --systohc

# ------------------------------------------------------
# Update reflector
# ------------------------------------------------------
echo "Start reflector..."
reflector -l 20 -p https -a 2 --sort rate --save /etc/pacman.d/mirrorlist

# ------------------------------------------------------
# Synchronize mirrors
# ------------------------------------------------------
pacman -Syy
pacman --noconfirm -S noto-fonts-emoji ttf-dejavu noto-fonts noto-fonts-extra efibootmgr networkmanager network-manager-applet bluez bluez-utils dosfstools mtools zip unzip brightnessctl inxi xdg-user-dirs pacman-contrib reflector bat htop duf refind
# ------------------------------------------------------
# set lang utf8 US
# ------------------------------------------------------
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
# ------------------------------------------------------
# Set hostname and localhost
# ------------------------------------------------------
echo "$hostname" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
# Set Root Password
# ------------------------------------------------------
echo "Set root password"
passwd root

# ------------------------------------------------------
# Add User
# ------------------------------------------------------
echo "Add user $username"
useradd -m -G wheel $username
passwd $username
echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/wheel

refind-install
systemctl enable NetworkManager.service
systemctl enable bluetooth
systemctl enable reflector.timer
systemctl enable fstrim.timer

xdg-user-dirs-update

echo "Done you can reboot now"
