#!/bin/bash
clear
echo "*PREREQUISITES*"
echo "make sure you have enough knowledge on installation process"
echo "even though this script has been made for non problematic installation"
echo "it is recommended that least research has been done so you do not have"
echo "any confusion during the selection of partitions which is very basic for"
echo "any OS installation"
echo "--> this installation script works with dual booting, so be careful when"
echo "    selecting ESP(EFI System Partiton) so that you wont lose booting to"
echo "    windows or any other OS that has been already installed"
echo "Use this script at your own risk"
echo ""
echo " INSTALLING ARCH..."
echo "-> uses ext4 as default format for install"
echo " * Zshell as default shell"
echo " * OhMyZsh for added customisation"
echo " * pyenv to manage python versions"
echo " * pyenv-virtualenv to manage virtual environments"
echo " * Kitty as default terminal"

#--------------------------------------------------
# Select partitions
#--------------------------------------------------
lsblk
read -p "Specify EFI partition:" efi
read -p "Specify root partition:" root

#--------------------------------------------------
# Sync time
#--------------------------------------------------
timedatectl set-ntp true

#--------------------------------------------------
# Format partitions
#--------------------------------------------------
mkfs.fat -F 32 /dev/$efi
mkfs.ext4 /dev/$root

#--------------------------------------------------
# Mount partitions
#--------------------------------------------------
mount /dev/$root /mnt
mkdir /mnt/efi
mount /dev/$efi /mnt/efi

echo ""
echo ""
lsblk
echo ""
echo ""

#--------------------------------------------------
# Installing base packages
#--------------------------------------------------
pacstrap -K /mnt base base-devel git linux linux-firmware vim openssh reflector rsync amd-ucode


#--------------------------------------------------
# Generate fstab
# ------------------------------------------------------
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab

cp 2-installuser.sh /mnt

arch-chroot /mnt
